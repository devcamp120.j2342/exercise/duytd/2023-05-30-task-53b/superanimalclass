import models.Animal;
import models.Cat;
import models.Dog;
import models.Mammal;

public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Tiger");
        System.out.println("Animal 1");
        System.out.println(animal1);

        Animal animal2 = new Animal("leopard");
        System.out.println("Animal 2");
        System.out.println(animal2);

        Mammal mammal1 = new Mammal("Lion");
        System.out.println("Mammal 1");
        System.out.println(mammal1);

        Mammal mammal2 = new Mammal("Panda");
        System.out.println("Mammal 2");
        System.out.println(mammal2);

        Cat cat1 = new Cat("Kitty");
        System.out.println("Cat 1");
        cat1.greets();
        System.out.println(cat1);

        Cat cat2 = new Cat("Meo tay");
        System.out.println("Cat 2");
        cat2.greets();
        System.out.println(cat2);

        Dog dog1 = new Dog("Corgi");
        System.out.println("Dog 1");
        dog1.greets();
        System.out.println(dog1);

        Dog dog2 = new Dog("Husky");
        System.out.println("Dog 2");
        dog2.greets();
        System.out.println(dog2);
    }
}
