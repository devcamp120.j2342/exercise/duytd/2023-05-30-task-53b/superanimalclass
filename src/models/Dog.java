package models;

public class Dog extends Mammal{

    public Dog(String name){
        super(name);
    }

    public void greets() {
        System.out.println("Woof");
        System.out.println("Wooooof");
    }

    @Override
    public String toString() {
        return String.format("Dog [Mammal[Animal[name = %s]]]", name);
    }

    
}
